package net.schjem.equipmentlistlab71.entities;

import com.google.gson.Gson;

import net.schjem.equipmentlistlab71.R;

public class Equipment {
    private long e_id;
    private String type;
    private String brand;
    private String model;
    private String description;
    private String it_no;
    private String aquired;
    private String image_url;

    public Equipment(long e_id, String type, String brand, String model, String description, String it_no, String acquired, String image_url) {
        this.e_id = e_id;
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.description = description;
        this.it_no = it_no;
        this.aquired = acquired;
        this.image_url = image_url;
    }

    public long getE_id() {
        return e_id;
    }

    public void setE_id(long e_id) {
        this.e_id = e_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIt_no() {
        return it_no;
    }

    public void setIt_no(String it_no) {
        this.it_no = it_no;
    }

    public String getAquired() {
        return aquired;
    }

    public void setAcquired(String acquired) {
        this.aquired = acquired;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static Equipment fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Equipment.class);
    }

    public static int iconType(String type) {
        switch (type) {
            default:
                return R.drawable.other;
            case "Laptop":
                return R.drawable.laptop;
            case "Nettbrett":
                return R.drawable.tablet;
            case "Smartklokke":
                return R.drawable.watch;
            case "PC":
            case "Skjerm":
                return R.drawable.pc;
            case "USBDocking":
                return R.drawable.dock;
            case "Oculus":
            case "Spill":
            case "Spillkonsoll":
                return R.drawable.gaming;
            case "Ruter":
                return R.drawable.router;
            case "Svitsj":
                return R.drawable.svitsj;
            case "Hub":
                return R.drawable.hub;
            case "TV-skjerm":
                return R.drawable.tv;
            case "HDMI-svitsj":
                return R.drawable.hdmi;
            case "Legosett":
                return R.drawable.lego;
        }
    }
}