package net.schjem.equipmentlistlab71.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.schjem.equipmentlistlab71.R;

public class MySettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}