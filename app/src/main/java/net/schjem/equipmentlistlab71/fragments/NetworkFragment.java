package net.schjem.equipmentlistlab71.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.schjem.equipmentlistlab71.entities.Equipment;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NetworkFragment extends Fragment {

    public interface DownloadCallback {
        void downloadComplete(ArrayList<Equipment> result);

        void downloadFailed(String errorMessage);
    }

    public static final String TAG = "NetworkFragment";
    public static final String URL_KEY = "UrlKey";

    private DownloadCallback mCallback = null;

    public static NetworkFragment getInstance(FragmentManager fragmentManager, String url) {
        NetworkFragment networkFragment = (NetworkFragment) fragmentManager
                .findFragmentByTag(NetworkFragment.TAG);

        if (networkFragment == null) {
            networkFragment = new NetworkFragment();

            Bundle args = new Bundle();
            args.putString(URL_KEY, url);
            networkFragment.setArguments(args);
            fragmentManager.beginTransaction().add(networkFragment, TAG).commit();
        }
        return networkFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //String mUrlString = getArguments() != null ? getArguments().getString(URL_KEY) : null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (DownloadCallback) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException(context.toString() + " må implementere NetworkFragment.DownloadCallback.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void doPostJSONRequest(String sortBy, String equipmentType, String which_equipment, String url, Context context) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("sortBy", sortBy);
            jsonBody.put("equipmentType", equipmentType);
            jsonBody.put("which_equipment", which_equipment);
        } catch (JSONException jse) {
            jse.printStackTrace();
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<Equipment>>() {
                        }.getType();
                        try {
                            ArrayList<Equipment> equipmentArrayList = gson.fromJson(jsonObject.getString("jsonResponse"), type);
                            mCallback.downloadComplete(equipmentArrayList);
                        } catch (JSONException error) {
                            mCallback.downloadFailed(error.toString());
                        }
                    }
                }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mCallback.downloadFailed(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                final Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }
}