package net.schjem.equipmentlistlab71.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.schjem.equipmentlistlab71.entities.Equipment;
import net.schjem.equipmentlistlab71.R;

public class DetailsActivity extends AppCompatActivity {
    Equipment equipment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        equipment = Equipment.fromJson(intent.getStringExtra("equipment"));

        TextView it_no = findViewById(R.id.detail_it_no);
        TextView type = findViewById(R.id.detail_type);
        TextView brand = findViewById(R.id.detail_brand);
        TextView model = findViewById(R.id.detail_model);
        TextView acquired = findViewById(R.id.detail_acquired);
        TextView description = findViewById(R.id.detail_description);
        ImageView icon = findViewById(R.id.detail_icon);
        ImageView image = findViewById(R.id.detail_image);

        it_no.setText(equipment.getIt_no());
        type.setText(equipment.getType());
        brand.setText(equipment.getBrand());
        model.setText(equipment.getModel());
        acquired.setText(equipment.getAquired());
        description.setText(equipment.getDescription());
        icon.setImageResource(Equipment.iconType(equipment.getType()));

        Picasso.get()
                .load(equipment.getImage_url())
                .into(image);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("equipment", equipment.toJson());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        equipment = Equipment.fromJson(savedInstanceState.getString("equipment"));
    }
}