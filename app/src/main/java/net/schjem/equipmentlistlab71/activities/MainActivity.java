package net.schjem.equipmentlistlab71.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import net.schjem.equipmentlistlab71.entities.Equipment;
import net.schjem.equipmentlistlab71.adapters.MyRecyclerViewAdapter;
import net.schjem.equipmentlistlab71.fragments.NetworkFragment;
import net.schjem.equipmentlistlab71.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, NavigationView.OnNavigationItemSelectedListener, MyRecyclerViewAdapter.ItemClickListener, NetworkFragment.DownloadCallback {
    private static final String URL = "http://kark.hin.no:8088/d3330log_backend/getTestEquipmentList";
    public static final String EXTRA_MESSAGE = "equipment";
    SwipeRefreshLayout swipeRefreshLayout;
    MyRecyclerViewAdapter adapter;
    RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpUIViews();
        doPostJSONRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            showSettings();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refrehData:
                doPostJSONRequest();
                break;
            case R.id.settings:
                showSettings();
                break;
            case R.id.aboutApp:
                showInfo();
                break;
            case R.id.exit:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        doPostJSONRequest();
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(EXTRA_MESSAGE, adapter.getItem(position).toJson());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void setUpUIViews() {

        //Preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Swipe down to refresh data
        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doPostJSONRequest();
            }
        });
    }

    public void setUpRecyclerView(ArrayList<Equipment> data) {
        int columns;
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            columns = 5;
        } else {
            columns = 3;
        }

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, columns));
        adapter = new MyRecyclerViewAdapter(this, data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    public void showSettings() {
        startActivity(new Intent(this, MySettingsActivity.class));
    }

    public void showInfo() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    public void doPostJSONRequest() {
        swipeRefreshLayout.setRefreshing(true);
        NetworkFragment networkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), URL);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        String sortBy = preferences.getString("sortBy", "type");
        String equipmentType = preferences.getString("filterType", "ALL");
        String which_equipment = preferences.getString("filterAvailable", "AVAILABLE");

        if (networkFragment != null)
            networkFragment.doPostJSONRequest(sortBy, equipmentType, which_equipment, URL, this.getApplicationContext());
    }

    @Override
    public void downloadComplete(ArrayList<Equipment> result) {
        Toast.makeText(this, getString(R.string.equipment_updated), Toast.LENGTH_LONG).show();
        setUpRecyclerView(result);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void downloadFailed(String errorMessage) {
        swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(this, getString(R.string.equipment_update_error), Toast.LENGTH_LONG).show();
    }
}