package net.schjem.equipmentlistlab71.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.schjem.equipmentlistlab71.R;
import net.schjem.equipmentlistlab71.entities.Equipment;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder> {

    private ArrayList<Equipment> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public MyRecyclerViewAdapter(Context context, ArrayList<Equipment> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Oppdater innholdet i enkeltcelle her!
        Equipment equipment = mData.get(position);
        String equipmentType = equipment.getType() + ", ";
        holder.type.setText(equipmentType);
        holder.brand.setText(equipment.getBrand());
        holder.it_no.setText(equipment.getIt_no());
        holder.icon.setImageResource(Equipment.iconType(equipment.getType()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView type;
        TextView brand;
        TextView it_no;
        ImageView icon;

        MyViewHolder(View itemView) {
            super(itemView);
            it_no = itemView.findViewById(R.id.rv_it_no);
            type = itemView.findViewById(R.id.rv_type);
            brand = itemView.findViewById(R.id.rv_brand);
            icon = itemView.findViewById(R.id.rv_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public Equipment getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}